using System;
using Xunit;

namespace UnitTests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            var expResult = 6;
            int a = 3;
            var actualResult = factorial(a);
            Assert.Equal(expResult, actualResult); 
        }

        [Theory]
        [InlineData(-5, 1)]
        public void Test3(int a, int expResult) {
            Assert.Equal(expResult, factorial(a));
        }

        int factorial(int a) {
            var output = a;
            if (a == 1 || a < 0 ) {
                return 1;
            } else {
                output = output * factorial(a-1);
            }
            return output;
        }
    }
}
