using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Web.Models;

namespace Web.Controllers
{
    public class BuildingController : Controller
    {
        
        public IActionResult Coldenhall()
        {
            return View();
        }

        public IActionResult GarrettStrong()
        {
            return Content("<html><body><h2>Garrett Strong</h2></body></html>");
        }

        public IActionResult Valkcenter()
        {
            return Content("This is Valk Center");
        }

        public IActionResult Studentunion()
        {
            return Json(new { name = "Building name", description="STUDENT UNION"});
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
